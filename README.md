:arrow_right: [See my blog post for some background](https://solarchemist.se/2022/05/20/ttrss-video-mpv)

# Play TinyTinyRSS video feeds using mpv

This is a small Python script wrapped by a bash script that
fetches your latest unread video feeds from a specific feed category in your
TinyTinyRSS account and plays them in `mpv`, and importantly, marks the feed articles
**as read** in TinyTinyRSS as you watch them.

I don't browse the video feeds in my RSS reader very often, mostly because it is
rather cumbersome to scroll through the list (even with the video embed plugin
enabled, because non-youtube embeds take a second to show up).

What if we could instead send that whole list of video URLs as a playlist to `mpv`?
After one video finishes, the next one in the feed would just start playing, and
so on?

To facilitate this, I have collected all video feeds in a single `category`
in my TinyTinyRSS (TTRSS) account.

+ https://tt-rss.org
+ https://mpv.io


## Usage

This script expects to find the password to your TTRSS account
in `~/.config/ttrss-mpv/config` (this path can be changed by editing the shell script).
At present this file is expected to contain only the password, nothing else.

```
taha@asks2:/media/bay/taha/local/git/ttrss-video-playlist
(master) $ ./ttrss-video-mpv.sh
```

+ if running for the first time, input your TTRSS account password at the prompt
+ the newest unread article (i.e., video) from your TTRSS category will start playing
  automatically. Use [`mpv` commands](https://mpv.io/manual/master) to control playback,
  skip to the next video, etc.
+ To stop playing, kill the terminal process/session.


## Install this script

Clone this repo and enter its directory
```{bash}
git clone https://codeberg.org/solarchemist/ttrss-video-mpv.git
cd ttrss-video-mpv
```

Run the provided `setup` shell script (please read through it first, it is quite simple)
to create a Python `venv` and install the required packages into it:
```{bash}
./setup-project.sh
```

You should now be able to run this script:
```{bash}
./ttrss-video-mpv.sh
```

Enjoy your uninterrupted video stream based on your own RSS feeds!



## Links and notes

+ https://stackoverflow.com/questions/3980668/how-to-get-a-password-from-a-shell-script-without-echoing
+ https://unix.stackexchange.com/questions/212183/how-do-i-check-if-a-variable-exists-in-an-if-statement
+ https://stackoverflow.com/questions/19075671/how-do-i-use-shell-variables-in-an-awk-script
+ https://stackoverflow.com/questions/14155669/call-python-script-from-bash-with-argument
+ https://stackoverflow.com/questions/89228/how-to-call-an-external-command
+ https://betterdev.blog/minimal-safe-bash-script-template
+ https://24ways.org/2013/keeping-parts-of-your-codebase-private-on-github



### Could we use this to also play podcasts?

If you, like me, collect podcast RSS feeds in their own category in TinyTinyRSS,
it becomes trivial to adapt this script (just change the `--category nn` value).

The problem is that some (most?) podcast URLs fail to resolve any playable media URL
when passed to `mpv`. Some do work though, so either `mpv` is doing some magic to
find the path of the MP3, or (more likely) those websites are employing some metadata
that `mpv` understands.

I would need to learn more to be able to figure out a suitable solution, I think.

+ [A Python script to download podcasts from an xml feed and tag the MP3s](https://gist.github.com/janetriley/7762552)
+ https://github.com/aziezahmed/podcast-player
+ https://github.com/jaseg/python-mpv



### How I installed ttrss-python

[`ttrss-python` is a TinyTinyRSS client library in Python, created by Markus Wiik](https://github.com/Vassius/ttrss-python). This libary does all the heavy lifting of authenticating and fetching
feeds/articles from your TinyTinyRSS instance.

+ Clone the repo: `git clone https://github.com/Vassius/ttrss-python.git`.
+ `cd` into the repo and setup virtualenv in it:

```
$ python3 -m venv env
$ source env/bin/activate
```

I dad to `pip3 install wheel` *before* installing the `ttrss` module, otherwise the
latter failed with `invalid command bdist_wheel`:
```
(env) taha@solarchemist:~/ttrss-python
$ pip3 install wheel
$ pip3 install ttrss-python
```

This is the awesome capabilities offered by the `ttrss-python` library:
```
>>> client.
client.assign_label(             client.get_pref(                 client.share_to_published(
client.catchup_feed(             client.get_unread_count(         client.sid
client.get_articles(             client.http_auth                 client.subscribe(
client.get_categories(           client.logged_in(                client.toggle_unread(
client.get_feed_count(           client.login(                    client.unsubscribe(
client.get_feed_tree(            client.logout(                   client.update_daemon_running(
client.get_feeds(                client.mark_read(                client.update_feed(
client.get_headlines(            client.mark_unread(              client.url
client.get_headlines_for_label(  client.password                  client.user
client.get_labels(               client.refresh_article(
```

To create/update the requirements file:
```
pip freeze > requirements.txt
```

To exit the Python env: `deactivate`.

I also found a [Gist that integrates TTRSS with Calibre](https://gist.github.com/oott123/d7833efcbf23ea604fb7),
which seems to contains its own Python code to interface with TTRSS.

+ https://github.com/Vassius/ttrss-python
+ https://stackoverflow.com/questions/41573587/what-is-the-difference-between-venv-pyvenv-pyenv-virtualenv-virtualenvwrappe
+ https://gist.github.com/oott123/d7833efcbf23ea604fb7
+ https://medium.com/@jtpaasch/the-right-way-to-use-virtual-environments-1bc255a0cba7
