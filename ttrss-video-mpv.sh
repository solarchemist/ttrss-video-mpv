#!/usr/bin/env bash
# December 2020
# Taha Ahmed


set -euo pipefail

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] -c/--category

Connects to your TinyTinyRSS instance and auto-plays all articles
from the category you specify using mpv. Works very well for videos
(thanks to mpv using yt-dlp behind the scenes) and for some podcasts.

Supply your TTRSS category id using the -c parameter, e.g., ttrss-video-mpv.sh -c 30
EOF
  exit
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
   while :; do
      case "${1-}" in
      -h | --help) usage ;;
      -v | --verbose) set -x ;;
      # your TTRSS "category id" that should be traversed by this script
      -c | --category)
         ttrss_catid="${2-}"
         shift
         ;;
      # URL to your TTRSS instance
      --url)
         ttrss_url="${2-}"
         shift
         ;;
      # your username on your TTRSS instance
      --user)
         ttrss_usr="${2-}"
         shift
         ;;
      # one of "config", "passwordstore", or "prompt" (default is "config")
      --passwordmethod)
         passwordmethod="${2-}"
         shift
         ;;
      # path to passwordstore file (only relevant if passwordmethod=="passwordstore"), for example:
      # --passfile ~/.password-store/hosts/taipei/ttrss/users/taha.gpg
      --passfile)
         pass_ttrss_path="${2-}"
         shift
         ;;
      -?*) die "Unknown option: $1" ;;
      *) break ;;
      esac
      shift
   done

   args=("$@")

   # Check required params and arguments
   [[ -z "${ttrss_catid-}" ]] && die "You have to provide the 'category' parameter"
   # If URL param was not provided, use this default
   [[ -z "${ttrss_url-}" ]] && ttrss_url="https://ttrss.chepec.se"
   # If username param was not provided, use this default
   [[ -z "${ttrss_usr-}" ]] && ttrss_usr="$USER"
   # If passwordmethod param was not provided, use this default
   [[ -z "${passwordmethod-}" ]] && passwordmethod="config"
   # apart from the above flags and parameters, this script takes no arguments
   [[ ${#args[@]} -gt 0 ]] && die "This script does not handle arguments"

   return 0
}

parse_params "$@"


### CONFIG FILE
# NOTE! We cannot use ~ (tilde) in path in bash, it is never expanded and leads
# to paths /home/user/~/.config/ttrss-mpv/config and other nonsense.
# I considered some alternatives, but I think the easiest is just to use the
# $HOME variable which is set by the shell, it is just what I want: the home
# directory of the user running this script
ttrss_mpv_config_path="$HOME/.config/ttrss-mpv/config"


if [[ $passwordmethod == "passwordstore" ]]; then
   # check if pass_ttrss_path variable is defined, is not empty string, and path points to a file
   # var is set AND var is not empty string AND path points to an existing file
   # since there is no point trying to fetch password if the path specified by the user does not exist
   if [[ ! -z ${pass_ttrss_path+x} && $pass_ttrss_path != "" && -f "$(eval echo $pass_ttrss_path)" ]]; then
      # strip "~/.password-store/" and ".gpg" from string
      # (room for improvement: handle full paths too, i.e., "/home/taha/..." instead of "~/")
      # bash string substitution: ${var/pattern/replacement}
      # https://wizardzines.com/comics/parameter-expansion/
      # note: hashtag here is equivalent to regex caret, ^
      pass_ttrss_file="${pass_ttrss_path/#\~\/\.password-store\//}"
      # note: percent here is equivalent to regex dollar, $
      pass_ttrss_file="${pass_ttrss_file/%\.gpg/}"
      # read the specified file in the user's Password Store
      pass_ttrss_file_contents=$(pass show $pass_ttrss_file)
      # get the first word (we're assuming that's the password) from the password-file
      # using awk and printf (this treats the variable as a file input)
      ttrss_pwd=$(printf '%s' "$pass_ttrss_file_contents" | awk 'NR==1{print $1}')
   else
      # in this case, prompt for the password
      # but just to be nice, if the user specified an invalid path in pass_ttrss_path, let them know
      if [[ ! -z ${pass_ttrss_path+x} && $pass_ttrss_path != "" && ! -f "$(eval echo $pass_ttrss_path)" ]]; then
         echo "\$pass_ttrss_path does not contain a path to an existing file. Did you mean to set it?"
      fi
      read -s -p "Password for $ttrss_usr at $ttrss_url: " ttrss_pwd
      printf "\n" # newline after read command to avoid messing up the output
   fi
   # run python script
   $script_dir/env/bin/python $script_dir/ttrss-video-mpv.py --url $ttrss_url --user $ttrss_usr --password $ttrss_pwd --category $ttrss_catid
elif [[ $passwordmethod == "prompt" ]]; then
   echo "What is the password for $ttrss_usr at $ttrss_url?"
   echo "Please note that the password will not be saved, but may be visible in the list of running processes."
   read -s -p "Password: " ttrss_pwd_prompt
   printf "\n" # newline after read command to avoid messing up the output
   # run python script
   $script_dir/env/bin/python $script_dir/ttrss-video-mpv.py --url $ttrss_url --user $ttrss_usr --password $ttrss_pwd --category $ttrss_catid
else
   # since we set "config" as default, we can assume that's the only possibility at this point
   # check if config file (supposed to contain password) exists on disk
   # https://stackoverflow.com/questions/10204562/difference-between-if-e-and-if-f
   if [ ! -f $ttrss_mpv_config_path ]; then
      # the config file does not exist, offer to create it
      msg "No config file was found. For your convenience, and to make future runs"
      msg "of ttrss-video-mpv easier, this script can create $ttrss_mpv_config_path for you."
      msg "What is the password for $ttrss_usr at $ttrss_url?"
      read -s -p "Password: " ttrss_pwd_prompt
      printf "\n" # newline after read command to avoid messing up the output
      # make sure the parent directory exists
      mkdir -p $(dirname "$ttrss_mpv_config_path")
      msg "Writing password to config file"
      printf "$ttrss_pwd_prompt" > $ttrss_mpv_config_path
      msg "Setting safer permissions on config file and parent directory"
      chmod 0700 $(dirname "$ttrss_mpv_config_path")
      chmod 0600 $ttrss_mpv_config_path
   fi
   # run python script without passing the password as argument (instead provide config path)
   $script_dir/env/bin/python $script_dir/ttrss-video-mpv.py --url "$ttrss_url" --user "$ttrss_usr" --category "$ttrss_catid" --config "$ttrss_mpv_config_path"
fi


# useful for debugging
# echo "\$ttrss_url: $ttrss_url"
# echo "\$ttrss_usr: $ttrss_usr"
# echo "\$ttrss_pwd: $ttrss_pwd"
# echo "\$ttrss_catid: $ttrss_catid"

return 0
