import sys
import subprocess
from ttrss.client import TTRClient
# getpass solely to getuser(), used to set a default for an arg, just a convenience
# getpass is part of the Python standard library
# https://docs.python.org/3/library/getpass.html
import getpass
# I assume this is best practice to check that a path exists
# as a nice bonus, Path also lets us copy text contents into a variable
# https://stackoverflow.com/a/49564464/1198249
# pathlib is part of the Python standard library
# https://docs.python.org/3/library/pathlib.html
from pathlib import Path
# to be able to pass *either* password in cleartext or path to config file that
# contains the password as arguments to this script, we need to use named args
# (can't use simply positional args any longer)
# the right tool for this job appears to be argparse, which comes in the standard library
# and replaces the older getopt and optparse modules
# https://realpython.com/command-line-interfaces-python-argparse
# https://www.digitalocean.com/community/tutorials/how-to-use-argparse-to-write-command-line-programs-in-python
# https://docs.python.org/3/library/argparse.html#action
import argparse

parser = argparse.ArgumentParser(
   description="Play videos from your TinyTinyRSS reader using mpv",
   formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
# category is a required argument, but I'm not really sure how to code that... perhaps revisit this later
parser.add_argument("--category", type=str, help="TTRSS category ID containing your video feeds")
parser.add_argument("--url", type=str, help="URL to your TTRSS instance")
# current $USER but in Python lingua?
# https://stackoverflow.com/questions/842059/is-there-a-portable-way-to-get-the-current-username-in-python
# https://www.w3resource.com/python-exercises/python-basic-exercise-54.php
parser.add_argument("--user", type=str, default=getpass.getuser(), help="your username on your TTRSS instance")
parser.add_argument("--password", type=str, default="", help="your password to your TTRSS account")
parser.add_argument("--config", type=str, default="", help="path to config file (containing your TTRSS password)")
args = parser.parse_args()

# fetch TTRSS account password from file on disk
# (note, NOT from passwordstore, because the whole point is to avoid the friction
# of having to unlock it just to play some videos)
# This approach should have the added benefit of not displaying the TTRSS password
# in the list of active processes
config_path = Path(args.config)
if args.config != "" and config_path.exists():
   # read file contents, which we assume is just the password
   ttrss_password = config_path.read_text()
elif args.password != "":
   ttrss_password = args.password
else:
   # throw an error saying "password must be set"
   # https://stackoverflow.com/a/22633601/1198249
   sys.exit("Sorry, cannot proceed without your password. Please specify either --config or --password.")

client = TTRClient(args.url, args.user, ttrss_password)
client.login()

categ30 = client.get_headlines(feed_id=args.category, is_cat=True, view_mode="unread", order_by="feed_dates")
# https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#getheadlines
# get_headlines() args:
# + feed_id – Feed id. This is available as the id property of a Feed object. Default is -4 (all feeds).
# + limit – Return no more than this number of headlines. Default is 0 (unlimited, though the server limits to 60).
# + skip – Skip this number of headlines. Useful for pagination. Default is 0.
# + is_cat – The feed_id is a category. Defaults to False.
# + show_excerpt – Include a short excerpt of the article. Defaults to True.
# + show_content – Include full article content. Defaults to False.
# + view_mode – (string = all_articles, unread, adaptive, marked, updated)
# + order_by – The default (None) actually uses "Default" sort. Alternative values are date_reverse (oldest first), and feed_dates (newest first ordered by feed date). This parameter requires ttrss-python > v0.5
# + include_attachments – include article attachments. Defaults to False.
# + since_id – Only include headlines newer than since_id.
# + include_nested – Include articles from child categories. Defaults to True.

# A single headline in the returned object contains:
# > print(dir(i))
# ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__',
#  '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__',
#  '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__',
#  '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
#  '__str__', '__subclasshook__', '__weakref__', '_client', 'always_display_attachments',
#  'author',           # channel name (string)
#  'comments_count',
#  'comments_link',
#  'content',
#  'excerpt',
#  'feed_id',
#  'feed_title',
#  'flavor_image',
#  'flavor_stream',
#  'full_article',
#  'guid',
#  'id',
#  'is_updated',
#  'labels',
#  'lang',
#  'link',             # video URL
#  'marked',
#  'note',
#  'published',        # corresponds to TTRSS "published article" by user (false/true)
#  'score',
#  'tags',
#  'title',            # video title (string)
#  'unread',
#  'updated']          # date and time string - conforms to video pubdate shown in TTRSS WEBUI, offset by one hour (but which is UTC and which is CET is not clear). Note: different than TTRSS "article import date", which can be seen when hovering over the date in TTRSS WEBUI. This pubdate agrees well with the date-time the video was shared on Youtube *for most videos*, but for some videos can be hours or even days later than the date shown on the Youtube website itself. Good tool to time and date (Youtube website only shows date) https://hadzy.com/analytics/youtube/

for i in categ30:
   # let's try to fail gracefully, for example if youtube video is private or otherwise duration not available
   # https://docs.python.org/3/tutorial/errors.html
   try:
      # check video duration and discard any that's shorter than 60 s (to get rid of TikTok-emulating "shorts")
      # print-json dumps a lot of video infor into the ytjson.stdout variable
      # https://stackoverflow.com/a/56893670/1198249
      ytcmd = "yt-dlp " + i.link + " --skip-download --print-json | jq .duration"
      # ytjson.stdout will contain the video length in seconds
      ytjson = subprocess.run([ytcmd], shell=True, stdout=subprocess.PIPE)
      ytlength = int(ytjson.stdout)
   except:
      # set ytlength to any value larger than the cutoff, simply to clear the if-else
      ytlength = 300
      print("Could not determine video duration, attempting to play it anyway")

   if ytlength > 60:
      print("")
      # print info to terminal output (mpv titlebar does not always include author, i.e., channel name)
      print("Now playing:", i.title)
      # https://stackoverflow.com/questions/19457227/how-to-print-like-printf-in-python3
      print("Now playing: {} ({})".format(i.author, i.updated))
      print("Now playing:", i.link)
      print("")
      # spawn mpv window with video, only return control here when mpv closes
      # as a work-around for all the settings resetting for each video, at least lower volume a little
      # keep-open=no forces the mpv window to close (and thus return control to this loop)
      # x11-name is a unique string which we use in our i3wm config to reset mpv window size and position
      subprocess.run(["mpv", i.link, "--volume=50", "--keep-open=no", "--x11-name=ttrss-video-mpv"])
      # mark this video as read in your TTRSS feed
      client.mark_read(i.id)
      # add a --- line surrounded by empty lines to give some visual separation between output
      print("\n---\n")
   else:
      print("")
      print("Now skipping:", i.title)
      print("Now skipping: {} ({})".format(i.author, i.updated))
      print("Now skipping:", i.link)
      print("Duration:", ytlength, "s")
      client.mark_read(i.id)
      #print("\n---\n")
