#!/usr/bin/env bash

set -euo pipefail
msg() {
  echo >&2 -e "setup-project.sh: ${1-}"
}

msg "Creating python3 venv"
python3 -m venv env

msg "Entering venv"
source env/bin/activate

msg "pip3 install wheel"
pip3 install wheel

msg "pip3 install requirements"
pip3 install -r requirements.txt
