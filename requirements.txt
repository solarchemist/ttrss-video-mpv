certifi==2020.11.8
chardet==3.0.4
idna==2.10
requests==2.25.0
# this is my fork of https://github.com/Vassius/ttrss-python
git+https://github.com/solarchemist/ttrss-python
urllib3==1.26.2
